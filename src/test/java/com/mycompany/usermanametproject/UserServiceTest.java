/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.usermanametproject;

import java.util.ArrayList;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author iHC
 */
public class UserServiceTest {
    UserService userService;
    public UserServiceTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
        userService = new UserService();       
        User newAdmin = new User("admin","Administator","pass@1234",'M','A');
        User newUser1 = new User("user1","User 1","pass@1234",'M','U');
        User newUser2 = new User("user2","User 2","pass@1234",'M','U');
        User newUser3 = new User("user3","User 3","pass@1234",'M','U');
        User newUser4 = new User("user4","User 4","pass@1234",'M','U');       
        userService.addUser(newAdmin);
        userService.addUser(newUser1);
        userService.addUser(newUser2);
        userService.addUser(newUser3);
        userService.addUser(newUser4);
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of addUser method, of class UserService.
     */
    @Test
    public void testAddUser() {
        System.out.println("addUser");
        User newUser = new User("admin","Administator","pass@1234",'M','A');
        UserService instance = new UserService();
        User expResult = newUser;
        User result = instance.addUser(newUser);
        assertEquals(expResult, result);
        assertEquals(1, result.getId());
    }

    /**
     * Test of getuser method, of class UserService.
     */
    @Test
    public void testGetuser() {
        System.out.println("getUser");
        int index = 1;
        String expResult = "User 1";
        User result = userService.getuser(index);
        assertEquals(expResult, result.getLogin());  
    }

    @Test
    public void testGetusers() {
        System.out.println("getusers");      
        ArrayList<User> userList = userService.getusers();
        assertEquals(5, userList.size());        
    }

    /**
     * Test of getsize method, of class UserService.
     */
    @Test
    public void testGetsize() {
        System.out.println("getsize");    
        int expResult = 5;
        int result = userService.getsize();
        assertEquals(expResult, result);      
    }

    /**
     * Test of logUserlist method, of class UserService.
     */
    @Test
    public void testLogUserlist() {
        System.out.println("logUserlist");
        UserService instance = new UserService();
        instance.logUserlist();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of updateUser method, of class UserService.
     */
    @Test
    public void testUpdateUser() {
        System.out.println("updateUser");
        int index = 0;
        User updateUser = null;
        UserService instance = new UserService();
        User expResult = null;
        User result = instance.updateUser(index, updateUser);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of deleteUser method, of class UserService.
     */
    @Test
    public void testDeleteUser() {
        System.out.println("deleteUser");
        int index = 0;
        UserService instance = new UserService();
        User expResult = null;
        User result = instance.deleteUser(index);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}
